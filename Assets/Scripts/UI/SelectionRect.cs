﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class SelectionRect : Singleton<SelectionRect>
{
    [HideInInspector]
    public float baseWidth, baseHeight;

    public List<Selectable> selected;
    
    public delegate void SelDelegate();
    public SelDelegate OnSelectionChanged;
    private void Awake() {
        // Find base width and height from image
        baseWidth = GetComponent<BoxCollider2D>().bounds.size.x;
        baseHeight = GetComponent<BoxCollider2D>().bounds.size.y;
        
        // Initialize list
        selected = new List<Selectable>();

        // Deactivate me
        gameObject.SetActive(false);
    }


    public void ClearSelection()
    {
        foreach(Selectable selectedObj in selected)
        {
            selectedObj.OnDeselect();
        }

        selected.Clear();
        if (OnSelectionChanged != null)
            OnSelectionChanged();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        // If other is selectable, add it to selection
        Selectable obj = other.transform.GetComponent<Selectable>();
        if(obj == null) return;

        obj.OnSelect();
        selected.Add(obj);
        if (OnSelectionChanged != null)
            OnSelectionChanged();
    }

    private void OnTriggerExit2D(Collider2D other) {
        // If it was selected and LB is still down, remove it from the selection
        if (!Input.GetMouseButton(0))
        {
            // Leave selection
            if (OnSelectionChanged != null)
                OnSelectionChanged();
            return;
        }
        Selectable obj = other.transform.GetComponent<Selectable>();
        if(obj == null) return;

        obj.OnDeselect();
        selected.Remove(obj);
        if (OnSelectionChanged != null)
            OnSelectionChanged();
    }


}
